# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 20:58:59 2020

@author: luckv
"""

import pandas as pd

def prepare_data(df: pd.DataFrame):
    return df

def load_local():
    df = pd.read_csv("world_dataset.csv")
    return prepare_data(df) 
    
def load_url():
    df = pd.read_csv("https://raw.githubusercontent.com/beoutbreakprepared/nCoV2019/master/latest_data/latestdata.csv")    
    return prepare_data(df)
