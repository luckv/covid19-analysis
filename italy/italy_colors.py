# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 15:46:12 2020

@author: luckv
"""

NEUTRAL = 'blue'
POPULATION = 'blue'
POSITIVE = 'yellow'
HOSPITALIZED = 'orange'
INTENSIVE_CARE = 'red'
DISCHARGED = 'green'
DECEASED = 'grey'