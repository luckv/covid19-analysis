# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:14:08 2020

@author: luckv
"""

import sys
sys.path.append('../')

import pandas as pd
#import matplotlib.pyplot as plt
#import seaborn as sns

import world_mobility

COUNTRY_CODE = 'IT'
GIORNI_SETTIMANA = world_mobility.WEEKDAYS_ITA

def load_data(local=False):
    df = world_mobility.load_data(local, 'IT')
    df = df.drop(columns = 'sub_region_2')
    return df

def national(df: pd.DataFrame):
    df = df[ df.sub_region_1.isna() ]
    df = df.drop(columns=['sub_region_1'])
    return df