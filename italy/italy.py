# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 23:34:50 2020

@author: luckv
"""

import sys

sys.path.append('../')

import pandas as pd
import dateutil.parser

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import italy_colors as colors
from world_population import COUNTRIES as WORLD_POPULATION

""" DATA """

ITALY_POPULATION = WORLD_POPULATION['italy']


def prepare_csv(df):
    del df["stato"]  # stato is ITA

    df.data = df.data.apply(lambda str: dateutil.parser.parse(str).date())

    df.sort_values('data', ascending=True, inplace=True)

    df.rename(columns={"tamponi": "totale_tamponi"}, inplace=True)

    df['nuovi_tamponi'] = df.totale_tamponi.diff()
    df.at[0, 'nuovi_tamponi'] = df.at[0, 'totale_tamponi']

    df['nuovi_dimessi_guariti'] = df.dimessi_guariti.diff()
    df.at[0, 'nuovi_dimessi_guariti'] = df.at[0, 'dimessi_guariti']

    df['nuovi_deceduti'] = df.deceduti.diff()
    df.at[0, 'nuovi_deceduti'] = df.at[0, 'deceduti']
    # Sometimes the differences of deceased are negative, should not happen.. may be there are zombies around
    df.nuovi_deceduti.where(lambda x: x > 0, other=0, inplace=True)

    df['nuovi_positivi'] = df.totale_positivi.diff() + df.nuovi_deceduti + df.nuovi_dimessi_guariti
    df.at[0, 'nuovi_positivi'] = df.at[0, 'totale_positivi']
    # Put a zero where negative values
    df.nuovi_positivi.where(lambda x: x > 0, other=0, inplace=True)

    df['nuovi_casi'] = df.totale_casi.diff() + df.nuovi_deceduti + df.nuovi_dimessi_guariti
    df.at[0, 'nuovi_casi'] = df.at[0, 'totale_casi']
    # Put a zero where negative values
    df.nuovi_casi.where(lambda x: x > 0, other=0, inplace=True)

    df.set_index('data', inplace=True)

    return df


def load_csv():
    return prepare_csv(pd.read_csv("datasets/italy.csv"))


def load_url():
    return prepare_csv(pd.read_csv(
        "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv"))


""" VISUALIZATION """

def get_plot_color(col_name: str):
    if (col_name == None):
        return colors.NEUTRAL

    if (col_name.startswith(('totale_', 'nuovi_', 'variazione_', 'stima_'))):
        col_name = col_name.split('_', 1)[1]

    if (col_name.endswith('tamponi')):
        return colors.POPULATION

    if (col_name == 'deceduti' or col_name.startswith('mortalità')):
        return colors.DECEASED

    if (col_name in ['dimessi_guariti', 'sopravvivenza']):
        return colors.DISCHARGED

    if (col_name.endswith('positivi') or col_name in ['isolamento_domiciliare', 'contagiati']):
        return colors.POSITIVE

    if (col_name.endswith('ospedalizzati') or col_name == 'ricoverati_con_sintomi'):
        return colors.HOSPITALIZED

    if (col_name == 'terapia_intensiva'):
        return colors.INTENSIVE_CARE

    return colors.NEUTRAL


def plot_colors(cols: list, overwrite=None):
    if (overwrite == None):
        return list(map(get_plot_color, cols))
    else:
        return list(
            map(lambda col_name: overwrite[col_name] if col_name in overwrite else get_plot_color(col_name), cols))


def setDayXaxis(ax):
    xaxis = ax.xaxis
    xaxis.set_major_locator(mdates.AutoDateLocator(minticks=3, maxticks=7))
    xaxis.set_minor_locator(mdates.DayLocator())
    xaxis.set_label_text('Data')
    xaxis.grid(b=True)


def prepare_plot():
    #axes = plt.axes()
    #setDayXaxis(axes)
    #axes.yaxis.grid(b=True)
    #plt.axes(axes)
    plt.grid()
    pass

def simple_plot(data, kind='line', colors=None):
    col_names = None

    if (isinstance(data, pd.DataFrame)):
        col_names = list(data.columns)
    elif isinstance(data, pd.Series):
        col_names = [data.name]

    if (colors == None or isinstance(colors, dict)):
        colors = plot_colors(col_names, overwrite=colors)

    data.plot(kind=kind, color=colors)
    prepare_plot()
    if (len(col_names) < 2):
        plt.legend().remove()
