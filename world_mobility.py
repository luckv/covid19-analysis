# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 10:07:09 2020

@author: luckv
"""
import pandas as pd
import numpy as np
from datetime import datetime
import re

DATASET_LOCAL = 'Global_Mobility_Report.csv'
DATASET_URL = 'https://www.gstatic.com/covid19/mobility/Global_Mobility_Report.csv'

def parse_date(string: str):
    return datetime.strptime(string, '%Y-%m-%d').date()

def rename_column(col_name: str):
    return re.sub('\._percent_change_from_baseline$', '', col_name)

def load_data(local = False, country_code = None):
    csv = pd.read_csv(DATASET_LOCAL if local else DATASET_URL,
                      dtype = {'country_region_code': 'str', 
                               'country_region': 'str',
                               'sub_region_1': 'str',
                               'sub_region_2': 'str' },
                      converters = { 'date': parse_date },
                      memory_map = local,
                      )
    
    if(country_code != None):
        csv = filter_by_country_code(csv, country_code, date_as_index = False)
        
    new_col_names = list(map( rename_column, csv.columns.values ))
    csv = csv.set_axis(axis=1, labels= new_col_names)
        
    return csv

def filter_by_country_code(df: pd.DataFrame, country_code: str, date_as_index = False):
    df = df[ df.country_region_code == country_code ].drop(columns=['country_region_code', 'country_region'])
    if(date_as_index):
        df = df.set_axis(df.date, axis='index')
        df = df.drop(columns = 'date')
        
    return df

def national_data(df: pd.DataFrame, country_code: str):
    nation_filter = True if country_code is None else df.country_region_code == country_code
    sub_region_filter = df.sub_region_1.isna()
    df = df[ np.logical_and(nation_filter, sub_region_filter) ]
    df = df.drop(columns=['country_region_code', 'country_region', 'sub_region_1', 'sub_region_2'])
    return df

def sub_region_1_data(df: pd.DataFrame, country_code: str):
    nation_filter = df.country_region_code == country_code
    sub_region_filter = df.sub_region_1.isna()
    df = df[ np.logical_and(nation_filter, np.logical_not(sub_region_filter)) ].drop(columns=['country_region_code', 'country_region', 'sub_region_2'])
    return df
    
def group_by(df: pd.DataFrame, level = 0):
    levels = ('country_region_code', 'sub_region_1', 'sub_region_2' )
    df = df.groupby(list(levels[0: level + 1]))
    return df

WEEKDAYS = ("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")
WEEKDAYS_ITA = ('Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato','Domenica')