import sys
sys.path.append('../')

import pandas as pd
from datetime import date

from world_population import COUNTRIES as POPULATIONS

def parse_date(string: str):
    splitted = list(map(int, string.split('/')))
    return date(splitted[2] + 2000, splitted[0], splitted[1])

def prepare_data(df: pd.DataFrame):
    df = df.drop(columns=['Province/State', 'Lat', 'Long']);
    df.rename(columns = {"Country/Region" : "country"}, inplace = True)
    df = df.groupby(['country']).sum()
    parsed_dates = list(map(parse_date, df.columns.values))
    df.set_axis(parsed_dates, axis='columns', inplace = True)
    return df

CSV_DIRECTORY = 'datasets_csv/'

CONFIRMED_LOCAL = CSV_DIRECTORY + 'confirmed.csv'
CONFIRMED_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
def load_confirmed(local = False):
    csv = pd.read_csv( CONFIRMED_LOCAL if local else CONFIRMED_URL )
    return prepare_data(csv)

def remove_US(df: pd.DataFrame):
    return df.drop(index='US')

def remove_columns_by_sum(df: pd.DataFrame, min_sum = None):
    if(min_sum is None):
        min_sum = -1
        
    columns = df.columns
    first_index = 0;
    columns_count = columns.size
    
    while( first_index < columns_count and df[columns.values[first_index]].sum() <= min_sum ):
        first_index+=1
        
    return df[columns[first_index:-1]]

def majors(df: pd.DataFrame):
    majors = df.sort_values(by = df.columns.values[-1], ascending=False).head()
    return remove_columns_by_sum(majors, min_sum=50)